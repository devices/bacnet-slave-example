package com.serotonin.bacnet4j.mytest;

import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.logging.Logger;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.RemoteDevice;
import com.serotonin.bacnet4j.RemoteObject;
import com.serotonin.bacnet4j.event.DeviceEventListener;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.mytest.exampleobject.AnalogInputExample;
import com.serotonin.bacnet4j.mytest.exampleobject.AnalogOutputExample;
import com.serotonin.bacnet4j.mytest.exampleobject.AnalogValueExample;
import com.serotonin.bacnet4j.mytest.exampleobject.BinaryInputExample;
import com.serotonin.bacnet4j.mytest.exampleobject.BinaryOutputExample;
import com.serotonin.bacnet4j.mytest.exampleobject.BinaryValueExample;
import com.serotonin.bacnet4j.mytest.exampleobject.MultistateInputExample;
import com.serotonin.bacnet4j.mytest.exampleobject.MultistateOutputExample;
import com.serotonin.bacnet4j.mytest.exampleobject.MultistateValueExample;
import com.serotonin.bacnet4j.npdu.ip.IpNetwork;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.obj.ObjectCovSubscription;
import com.serotonin.bacnet4j.service.confirmed.ReinitializeDeviceRequest.ReinitializedStateOfDevice;
import com.serotonin.bacnet4j.transport.Transport;
import com.serotonin.bacnet4j.type.Encodable;
import com.serotonin.bacnet4j.type.constructed.Choice;
import com.serotonin.bacnet4j.type.constructed.DateTime;
import com.serotonin.bacnet4j.type.constructed.PropertyValue;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.ServicesSupported;
import com.serotonin.bacnet4j.type.constructed.TimeStamp;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.enumerated.EventState;
import com.serotonin.bacnet4j.type.enumerated.EventType;
import com.serotonin.bacnet4j.type.enumerated.MessagePriority;
import com.serotonin.bacnet4j.type.enumerated.NotifyType;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.notificationParameters.NotificationParameters;
import com.serotonin.bacnet4j.type.primitive.Boolean;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;


public class MySlaveDevice
{
	private static final Logger log = Logger.getLogger(MySlaveDevice.class.getName());
		
	public MySlaveDevice() throws Exception
	{
		LocalDevice slaveDevice = new LocalDevice(1234, new Transport(new IpNetwork(getBroadcastAddress())));
//		LocalDevice slaveDevice = new LocalDevice(1234, new Transport(new IpNetwork()));
		slaveDevice.getConfiguration().setProperty(PropertyIdentifier.objectName, new CharacterString("MySlaveDevice"));
		slaveDevice.getEventHandler().addListener(new Listener());
		
		//add Objects
		slaveDevice.addObject(new AnalogInputExample(slaveDevice));
		slaveDevice.addObject(new AnalogOutputExample(slaveDevice));
		slaveDevice.addObject(new AnalogValueExample(slaveDevice));
		
		slaveDevice.addObject(new BinaryInputExample(slaveDevice));
		slaveDevice.addObject(new BinaryOutputExample(slaveDevice));
		slaveDevice.addObject(new BinaryValueExample(slaveDevice));
		
		slaveDevice.addObject(new MultistateInputExample(slaveDevice));
		slaveDevice.addObject(new MultistateOutputExample(slaveDevice));
		slaveDevice.addObject(new MultistateValueExample(slaveDevice));
		
//		slaveDevice.addObject(new BitstringValueExample(slaveDevice));
//		slaveDevice.addObject(new CharacterstringValueExample(slaveDevice));
//		slaveDevice.addObject(new OctetstringValueExample(slaveDevice));
//		slaveDevice.addObject(new IntegerValueExample(slaveDevice));
//		slaveDevice.addObject(new PositivIntegerValueExample(slaveDevice));

//		ServicesSupported servicesSup = slaveDevice.getServicesSupported();
//		servicesSup.setSubscribeCovProperty(true);
//		slaveDevice.getConfiguration().setProperty(PropertyIdentifier.protocolServicesSupported, servicesSup);
		
		slaveDevice.initialize();
		

		final BACnetObject analogIn = slaveDevice.getObject("G1-RLT03_TM-01");
		final BACnetObject binaryIn = slaveDevice.getObject("G1-HZG05-F1001");
		
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	      	  float value = 15.0f;
	      	  float increment = 0.1f;
	      	  while(true)
	      	  {
	      		  value = value + increment;
	      		  if(value > 35.0f)
	      			  value = 15.0f;
	      		  try
	      		  {
	      			  analogIn.setProperty(PropertyIdentifier.presentValue, new Real(value));
	      			  
	      			  Thread.sleep(1000);
	      		  } catch (BACnetServiceException | InterruptedException e)
	      		  {
	      			  e.printStackTrace();
	      		  }
	      	  }
	        }
		}).start();
		
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	      	  int value = 1;
	      	  while(true)
	      	  {
	      		  if (value == 1)
	      		  {
	      			  value = 0;
	      		  }
	      		  else
	      		  {
	      			  value = 1;
	      		  }
	      		  
	      		  try
	      		  {
	      			  binaryIn.setProperty(PropertyIdentifier.presentValue, new BinaryPV(value));
	      			  
	      			  Thread.sleep(7292);
	      		  } catch (BACnetServiceException | InterruptedException e)
	      		  {
	      			  e.printStackTrace();
	      		  }
	      	  }
	        }
		}).start();
	}
	
	static class Listener implements DeviceEventListener
	{

		@Override
		public boolean allowPropertyWrite(BACnetObject arg0, PropertyValue arg1)
		{
			if (arg0.getId().getObjectType().equals(ObjectType.analogOutput) && arg1.getPropertyIdentifier().equals(PropertyIdentifier.presentValue))
				return true;
				
			if (arg0.getId().getObjectType().equals(ObjectType.analogOutput) && arg1.getPropertyIdentifier().equals(PropertyIdentifier.maxPresValue))
				return true;
			
			return false;
		}

		@Override
		public void covNotificationReceived(UnsignedInteger arg0,
				RemoteDevice arg1, ObjectIdentifier arg2, UnsignedInteger arg3,
				SequenceOf<PropertyValue> arg4)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void eventNotificationReceived(UnsignedInteger arg0,
				RemoteDevice arg1, ObjectIdentifier arg2, TimeStamp arg3,
				UnsignedInteger arg4, UnsignedInteger arg5, EventType arg6,
				CharacterString arg7, NotifyType arg8, Boolean arg9,
				EventState arg10, EventState arg11, NotificationParameters arg12)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void iAmReceived(RemoteDevice arg0)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void iHaveReceived(RemoteDevice arg0, RemoteObject arg1)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void listenerException(Throwable arg0)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void privateTransferReceived(UnsignedInteger arg0,
				UnsignedInteger arg1, Encodable arg2)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void propertyWritten(BACnetObject arg0, PropertyValue arg1)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void reinitializeDevice(ReinitializedStateOfDevice arg0)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void synchronizeTime(DateTime arg0, boolean arg1)
		{
			// TODO Auto-generated method stub
			
		}

		@Override
		public void textMessageReceived(RemoteDevice arg0, Choice arg1,
				MessagePriority arg2, CharacterString arg3)
		{
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private String getBroadcastAddress()
	{
		System.setProperty("java.net.preferIPv4Stack", "true");
		NetworkInterface netif = null;
		
		String interfaceName = "eth";
		Integer interfaceNumber = 0;
		
		do
		{
			try
			{
				netif = NetworkInterface.getByName(interfaceName + interfaceNumber);
				log.info("new interface:" + netif.getDisplayName());
			
				InterfaceAddress ifAddr = netif.getInterfaceAddresses().get(0);
				log.info("address:" + ifAddr.getAddress().getHostAddress() 
						+ " name:" + ifAddr.getAddress().getHostName()
						+ " prefix:" + ifAddr.getNetworkPrefixLength()
						+ " broadcast:" + ifAddr.getBroadcast());
			
				String broadcastAddress = ifAddr.getBroadcast().toString();
				return broadcastAddress.substring(1);
			
			} catch (Exception e)
			{
				log.info("Looking for another Interface!\n");
				//e.printStackTrace();
			}
			
			interfaceNumber++;
			
			if (interfaceNumber == 11)
			{
				interfaceName = "en";
				interfaceNumber = 0;
			}
			
		} while ((!interfaceName.contentEquals("en")) && (interfaceNumber <= 10));
		
		return "255.255.255.255";
	}
	
	public static void main (String[] args) throws Exception
	{
		@SuppressWarnings("unused")
		MySlaveDevice test = new MySlaveDevice();
	}
}
