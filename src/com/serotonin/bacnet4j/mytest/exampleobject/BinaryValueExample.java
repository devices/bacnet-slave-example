package com.serotonin.bacnet4j.mytest.exampleobject;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;

public class BinaryValueExample extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BinaryValueExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.binaryValue));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RLT04-V1"));
		this.setProperty(PropertyIdentifier.presentValue, BinaryPV.active);
		this.setProperty(PropertyIdentifier.description, new CharacterString("Gesamtanlage Ebene 4 Flurlüftung"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.activeText, new CharacterString("Fern aus"));
		this.setProperty(PropertyIdentifier.inactiveText, new CharacterString("Fern ein"));
	}
}
