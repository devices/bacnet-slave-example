package com.serotonin.bacnet4j.mytest.exampleobject;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.LimitEnable;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class AnalogOutputExample extends BACnetObject
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AnalogOutputExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.analogOutput));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RLT03_Y1-01"));
		this.setProperty(PropertyIdentifier.presentValue, new Real(75.0f));
		this.setProperty(PropertyIdentifier.description, new CharacterString("Klappenantrieb"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.units, EngineeringUnits.percent);
		this.setProperty(PropertyIdentifier.minPresValue, new Real(0.0f));
		this.setProperty(PropertyIdentifier.maxPresValue, new Real(100.0f));
		this.setProperty(PropertyIdentifier.resolution, new Real(0.1f));
	}

}
