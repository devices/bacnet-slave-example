package com.serotonin.bacnet4j.mytest.exampleobject;

import java.util.ArrayList;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class MultistateInputExample extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MultistateInputExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.multiStateInput));
		
		ArrayList<CharacterString> states = new ArrayList<CharacterString>();
		states.add(new CharacterString("Hand"));
		states.add(new CharacterString("Aus"));
		states.add(new CharacterString("Auto"));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-TWW01-S2-H-O-A"));
		this.setProperty(PropertyIdentifier.presentValue, new UnsignedInteger(1));
		this.setProperty(PropertyIdentifier.description, new CharacterString("Hand-Aus-Automatik"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.numberOfStates, new UnsignedInteger(3));
		this.setProperty(PropertyIdentifier.stateText, new SequenceOf<CharacterString>(states));
	}
}
