package com.serotonin.bacnet4j.mytest.exampleobject;

import java.util.ArrayList;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class MultistateValueExample extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MultistateValueExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.multiStateValue));
		
		ArrayList<CharacterString> states = new ArrayList<CharacterString>();
		states.add(new CharacterString("Economy"));
		states.add(new CharacterString("Precomfort"));
		states.add(new CharacterString("Comfort"));
		states.add(new CharacterString("Protection"));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RAU99_FanCoil20"));
		this.setProperty(PropertyIdentifier.presentValue, new UnsignedInteger(2));
		this.setProperty(PropertyIdentifier.description, new CharacterString("Geb.1 Raum 99 FanCoil20"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.numberOfStates, new UnsignedInteger(4));
		this.setProperty(PropertyIdentifier.stateText, new SequenceOf<CharacterString>(states));
	}
}
