package com.serotonin.bacnet4j.mytest.exampleobject;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.Polarity;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;

public class BinaryOutputExample extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BinaryOutputExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.binaryOutput));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RLT03-M2-01"));
		this.setProperty(PropertyIdentifier.presentValue, BinaryPV.inactive);
		this.setProperty(PropertyIdentifier.description, new CharacterString("Fortl�fter Flur 3 Sozialr�ume"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.polarity, Polarity.reverse);
		this.setProperty(PropertyIdentifier.activeText, new CharacterString("Ventilator ein"));
		this.setProperty(PropertyIdentifier.inactiveText, new CharacterString("Ventilator aus"));
	}
}
