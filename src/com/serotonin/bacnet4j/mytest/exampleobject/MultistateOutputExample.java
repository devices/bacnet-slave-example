package com.serotonin.bacnet4j.mytest.exampleobject;

import java.util.ArrayList;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.SequenceOf;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class MultistateOutputExample extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MultistateOutputExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.multiStateOutput));
		
		ArrayList<CharacterString> states = new ArrayList<CharacterString>();
		states.add(new CharacterString("Aus"));
		states.add(new CharacterString("Stufe1"));
		states.add(new CharacterString("Stufe2"));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RLT01-M1-2StufVentilator"));
		this.setProperty(PropertyIdentifier.presentValue, new UnsignedInteger(2));
		this.setProperty(PropertyIdentifier.description, new CharacterString("Geb.1 L�ftung Zul�fter Stufe 1-2"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.numberOfStates, new UnsignedInteger(3));
		this.setProperty(PropertyIdentifier.stateText, new SequenceOf<CharacterString>(states));
	}
}
