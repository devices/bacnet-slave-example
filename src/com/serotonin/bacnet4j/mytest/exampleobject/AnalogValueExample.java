package com.serotonin.bacnet4j.mytest.exampleobject;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.LimitEnable;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.ObjectIdentifier;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class AnalogValueExample extends BACnetObject
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AnalogValueExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.analogValue));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RLT03_V2-01"));
		this.setProperty(PropertyIdentifier.presentValue, new Real(10.1f));
		this.setProperty(PropertyIdentifier.description, new CharacterString("Enthalpi-Berechnung"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.units, EngineeringUnits.joulesPerKilogramDryAir);

	}

}
