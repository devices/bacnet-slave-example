package com.serotonin.bacnet4j.mytest.exampleobject;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.LimitEnable;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.EngineeringUnits;
import com.serotonin.bacnet4j.type.enumerated.EventState;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.enumerated.Reliability;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.Real;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class AnalogInputExample extends BACnetObject
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AnalogInputExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.analogInput));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-RLT03_TM-01"));
		this.setProperty(PropertyIdentifier.presentValue, new Real(20.1f));
		this.setProperty(PropertyIdentifier.description, new CharacterString("Mischluft_Temperatur"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(false, false, false, false));
		this.setProperty(PropertyIdentifier.updateInterval, new UnsignedInteger(10));
		this.setProperty(PropertyIdentifier.units, EngineeringUnits.degreesCelsius);
		this.setProperty(PropertyIdentifier.minPresValue, new Real(-30.0f));
		this.setProperty(PropertyIdentifier.maxPresValue, new Real(50.0f));
		this.setProperty(PropertyIdentifier.resolution, new Real(0.1f));
		this.setProperty(PropertyIdentifier.covIncrement, new Real(0.5f));
		this.setProperty(PropertyIdentifier.timeDelay, new UnsignedInteger(10));
		this.setProperty(PropertyIdentifier.highLimit, new Real(26.0f));
		this.setProperty(PropertyIdentifier.lowLimit, new Real(17.0f));
		this.setProperty(PropertyIdentifier.limitEnable, new LimitEnable(true, true));	
	}

}
