package com.serotonin.bacnet4j.mytest.exampleobject;

import com.serotonin.bacnet4j.LocalDevice;
import com.serotonin.bacnet4j.exception.BACnetServiceException;
import com.serotonin.bacnet4j.obj.BACnetObject;
import com.serotonin.bacnet4j.type.constructed.StatusFlags;
import com.serotonin.bacnet4j.type.enumerated.BinaryPV;
import com.serotonin.bacnet4j.type.enumerated.ObjectType;
import com.serotonin.bacnet4j.type.enumerated.Polarity;
import com.serotonin.bacnet4j.type.enumerated.PropertyIdentifier;
import com.serotonin.bacnet4j.type.primitive.CharacterString;
import com.serotonin.bacnet4j.type.primitive.UnsignedInteger;

public class BinaryInputExample extends BACnetObject
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public BinaryInputExample(LocalDevice locDev) throws BACnetServiceException
	{
		super(locDev, locDev.getNextInstanceObjectIdentifier(ObjectType.binaryInput));
		
		this.setProperty(PropertyIdentifier.objectName, new CharacterString("G1-HZG05-F1001"));
		this.setProperty(PropertyIdentifier.presentValue, BinaryPV.active);
		this.setProperty(PropertyIdentifier.description, new CharacterString("Penthouse Vorlauf stat. Druck"));
		this.setProperty(PropertyIdentifier.statusFlags, new StatusFlags(true, false, false, false));
		this.setProperty(PropertyIdentifier.polarity, Polarity.reverse);
		this.setProperty(PropertyIdentifier.activeText, new CharacterString("Wassermangel"));
		this.setProperty(PropertyIdentifier.inactiveText, new CharacterString("Statischer Druck normal"));
		//this.setProperty(PropertyIdentifier.timeDelay, new UnsignedInteger(10));
	}
}
